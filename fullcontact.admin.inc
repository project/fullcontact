<?php

/**
 * @file
 * This is Full Contact Setting Form.
 */

/**
 * Implements fullcontact_admin_settings().
 */
function fullcontact_admin_settings() {
  $_socialarray = _fullcontact_get_social_array();
  $form = array();
  $form['fullcontact_description'] = array(
    '#type' => 'item',
    '#markup' => '<div><div><strong>' . t("Intructions to get the FullContact Api ID.") . '</strong></div><ul><li>' . t("Please read document to create fullcontact api <a href='https://www.fullcontact.com/developer/docs/' target='_blank'>click here</a>") . '</li> <li>' . t('To get FullContact Api please create an account on the <a href="https://www.fullcontact.com/developer/try-fullcontact/" target="_blank">fullcontact.com</a>') . '</li><li>' . t("You will get an api of Fullcontact on your registered email id.") . '</li><li>' . t("Please Fill the Fullcontact Api Id in the FullContact API ID field.") . '</li></div>',
  );
  $form['fullcontact_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('FullContact Api Setting'),
    '#collapsible' => TRUE,
  );
  $form['fullcontact_settings']['fullcontact_api'] = array(
    '#type' => 'textfield',
    '#title' => t('FullContact APP ID'),
    '#required' => TRUE,
    '#default_value' => variable_get('fullcontact_api'),
  );

  $form['fullcontact_social_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('FullContact Social Enable/Disable'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  $social_setting = variable_get('fullcontact_social_settings');
  
  if (is_array($social_setting)) {
  foreach ($_socialarray as $key => $value) {
    $form['fullcontact_social_settings'][$key] = array(
      '#type' => 'checkbox',
      '#title' => '<strong>' . $value . '</strong>',
      '#default_value' => $social_setting[$key],
    );
  } }
  return system_settings_form($form);
}
