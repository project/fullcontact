FULL CONTACT
----------------


CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Features
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

 * “Full Contact” module fetch social profile links from the Full Contact based on user email, and display the links under user profile in the Social tab.


REQUIREMENTS
------------

 * Curl should be installed. 


FEATURES
--------

 * The Full Contact module provide the Social profiles link of a specific user based on email id.
 * Full Contact API ID option in admin.
 * Social Tab on profile page for the user to see the all profile links.
 * Profile field editable.
 * "Full Contact update" button on the edit profile form.
 * Option to hide/show the social link by admin for all users.


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 
 * Get the FullContact API ID after registering on the http://www.fullcontact.com
 * Fill the FullContact API ID in admin using this url as: admin/config/people/fullcontact
 * select the FullContact Social Enable/Disable option to display the profile links and then save configauration.


TROUBLESHOOTING
---------------

 * If the profiles does not display, check the following:

   admin/config/people/fullcontact		

   - Check full contact socials enable in full contact setting.
   - Check full contact api key in setting.


MAINTAINERS
-----------

Current maintainers:

 * Manish Mittal (manishmittal) - https://www.drupal.org/u/manishmittal
 * Durgesh Soni (durgesh) - https://www.drupal.org/u/durgesh
 * Jitendra Kumar Sharma (jitendra0586) - https://www.drupal.org/u/jitendra0586



