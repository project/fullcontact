<?php

/**
 * @file
 * Intiliase the module.
 */

/**
 * Implement the fullcontact_init.
 */
function _fullcontact_get_social_array() {
  array(
    'fullcontact_facebook' => t('Facebook'),
    'fullcontact_twitter' => t('Twitter'),
    'fullcontact_pinterest' => t('Pinterest'),
    'fullcontact_googleplus' => t('Google Plus'),
    'fullcontact_flickr' => t('Flickr'),
    'fullcontact_youtube' => t('Youtube'),
    'fullcontact_klout' => t('Klout'),
    'fullcontact_gravatar' => t('Gravatar'),
    'fullcontact_vimeo' => t('Vimeo'),
    'fullcontact_myspace' => t('Myspace'),
    'fullcontact_foursquare' => t('Foursquare'),
    'fullcontact_linkedin' => t('Linkedin'),
    'fullcontact_quora' => t('Quora'),
  );
}

/**
 * Implements hook_help().
 */
function fullcontact_help($path, $arg) {
  switch ($path) {
    case 'admin/help#fullcontact':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The FullContact module allow user to display their social links.') . '</p>';
      return $output;

  }
}

/**
 * Implements hook_menu().
 */
function fullcontact_menu() {
  $items['admin/config/people/fullcontact'] = array(
    'title' => 'Full Contact Settings',
    'description' => 'Configure FullContact Settings Need fullcontact api',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('fullcontact_admin_settings'),
    'access arguments' => array('administer users'),
    'file' => 'fullcontact.admin.inc',
    'weight' => 20,
  );
  $items['user/%user/social'] = array(
    'title' => 'Social',
    'page callback' => 'fullcontact_social',
    'page arguments' => array(1),
    'access callback' => 'user_view_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Implements hook_form_alter().
 */
function fullcontact_form_alter(&$form, &$form_state, $form_id) {
  $_socialarray = _fullcontact_get_social_array();
  switch ($form_id) {
    case 'user_profile_form':
      $form['fullcontact_social'] = array(
        '#type' => 'fieldset',
        '#title' => t('FullContact Social Profiles'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      foreach ($_socialarray as $key => $value) {
        $form['fullcontact_social']['field_' . $key] = $form['field_' . $key];
        $form['fullcontact_social']['field_' . $key] = $form['field_' . $key];
        unset($form['field_' . $key]);
      }
      $form['actions']['fullcontact_reset'] = array(
        '#type' => 'submit',
        '#value' => t('Update Social Profiles'),
        '#weight' => 15,
        '#submit' => array('fullcontact_update'),
      );
      return $form;
  }
}

/**
 * Implements hook_user_login().
 */
function fullcontact_user_login(&$edit, $account) {
  $fc_appid = variable_get('fullcontact_api');
  if (empty($fc_appid)) {
    drupal_set_message(t('Please check FullContact APP ID.'), 'error');
  }
  else {
    if (empty($account->field_facebook) && empty($account->twitter) && empty($account->pinterest) && empty($account->googleplus) && empty($account->flickr) && empty($account->youtube) && empty($account->klout) && empty($account->gravatar) && empty($account->vimeo) && empty($account->myspace) && empty($account->foursquare) && empty($account->linkedin) && empty($account->quora)) {
      $response = fullcontact_curl_call($account, $fc_appid);

      if ($response['status'] === 200 && !empty($response['socialProfiles']) && is_array($response['socialProfiles'])) {
        foreach ($response['socialProfiles'] as $socialprofile) {
          $sfield = 'field_fullcontact_' . $socialprofile['typeId'];
          if (field_info_instance('user', $sfield, 'user')) {
            $loadaccount = user_load($account->uid);
            $edit = array(
              $sfield => array(
                'und' => array(
                  0 => array(
                    'value' => $socialprofile['url'],
                  ),
                ),
              ),
            );
            user_save($loadaccount, $edit);
          }
        }

        drupal_set_message(t('Social Profiles  imported successfully.'), 'status');
      }
      else {
        drupal_set_message(t('Nothing found on full contact.'), 'error');
      }
    }
  }
}

/*
 *  this function call when the reset fullcontact
 *
 * */

/**
 * Implements hook_update().
 */
function fullcontact_update($form_id, &$form_state) {
  global $user;
  $fc_appid = variable_get('fullcontact_api');

  if (empty($fc_appid)) {
    drupal_set_message(t('Please check FullContact APP ID.'), 'error');
  }
  else {
    $response = fullcontact_curl_call($user, $fc_appid);
    if ($response['status'] === 200 && !empty($response['socialProfiles']) && is_array($response['socialProfiles'])) {
      foreach ($response['socialProfiles'] as $socialprofile) {
        $sfield = 'field_fullcontact_' . $socialprofile['typeId'];
        if (field_info_instance('user', $sfield, 'user')) {
          $loadaccount = user_load($user->uid);
          $edit = array(
            $sfield => array(
              'und' => array(
                0 => array(
                  'value' => $socialprofile['url'],
                ),
              ),
            ),
          );
          user_save($loadaccount, $edit);
        }
      }

      drupal_set_message(t('Social Profile  imported successfully.'), 'status');
    }
    else {
      drupal_set_message(t('Nothing found on full contact.'), 'error');
    }
  }
}

/**
 * Curl call here.
 */
function fullcontact_curl_call($user, $fc_appid) {
  $ch = curl_init();
  $url = "https://api.fullcontact.com/v2/person.json?email=" . $user->mail . "&apiKey=" . $fc_appid;
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  $output = curl_exec($ch);
  $response = json_decode($output, TRUE);
  curl_close($ch);
  return $response;
}

/**
 * Page callback wrapper for fullcontact_social().
 */
function fullcontact_social($account) {

  return fullcontact_view($account);

}

/**
 * This function is used to show the Social tab.
 */
function fullcontact_view($account) {
  $_socialarray = _fullcontact_get_social_array();
  $display_option = variable_get('fullcontact_social_settings');
  $user_fields = user_load($account->uid);
  $header = array(t('Social Name'), t('Social Link'));
  $rows = array();
  if (is_array($_socialarray) && !empty($_socialarray)) {
    if (in_array(1, $display_option)) {
      foreach ($_socialarray as $key => $value) {
        if ($display_option[$key] == 1) {
          $field = 'field_' . $key;
          $field_value = $user_fields->$field;
          if ($field_value) {
            $field_value = reset($field_value);
            $field_value = $field_value[0]['value'];
            $rows = array(check_plain($value),
              l($field_value, $field_value, array('html' => TRUE, 'attributes' => array('target' => '_blank'))),
            );
          }
        }
      }
    }
    else {
      foreach ($_socialarray as $key => $value) {
        $field = 'field_' . $key;
        $field_value = $user_fields->$field;
        if ($field_value) {
          $field_value = reset($field_value);
          $field_value = $field_value[0]['value'];
          $rows[] = array(check_plain($value),
            l($field_value, $field_value, array('html' => TRUE, 'attributes' => array('target' => '_blank'))),
          );
        }
      }
    }
  }
  else {
    drupal_set_message(t('No entries meet the filter criteria'));
  }
  return theme('table', array('header' => $header, 'rows' => $rows));
}
